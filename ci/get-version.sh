#!/bin/bash
usage() {
    echo "Usage: $0"
    exit 1
}
# check no arguments are passed if so show usage
[[ $# -ne 0 ]] && usage

# check maven
mvn -v || { echo "Could not launch Maven. Make sure it's installed correctly" && exit 1; }

#export PROJECT_BUILD_NAME="$(mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.build.finalName | grep -Ev '(^\[|Download\w+:)')"
#echo "Project build name is ${PROJECT_BUILD_NAME}"
export PROJECT_VERSION="$(mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | sed -n -r  -e '/^[.*]/ !{ /^([0-9]{1,3}\.)*[0-9]{1,5}(-SNAPSHOT|$)$/ { p; q } }')"

[[ -z $PROJECT_VERSION ]] && { echo "Unable to determite the PROJECT_VERSION. Exiting." ; exit 1; }
echo "Project version after script is ${PROJECT_VERSION}"
