#!/usr/bin/env bash
# Script to run the Maven install phase and putting the artifacts to local Maven repository
usage() {
	echo "Usage: $0 test/build"
	exit 1
}
# check no arguments are passed if so show usage
[[ $# -ne 1 ]] && usage
# Invoke Maven install phase
echo Project version is ${PROJECT_VERSION}
if [ "test" == ${1} ]; then
    mvn test || { echo "Maven unit tests failed" && exit 1; }
fi
if [ "build" == ${1} ]; then
    mvn package -Dmaven.test.skip=true || { echo "Maven build failed" && exit 1; }
    
    CHECKPATH=`pwd`/target
    CHECKNAME=${CI_PROJECT_NAME}-${PROJECT_VERSION}.jar
    echo "[INFO] check the final name to equal the template name ${CHECKNAME}"
    if [ ! -f ${CHECKPATH}/${CHECKNAME} ]; then
        echo "[ERROR] The final name of jar must be equal with ${CHECKNAME}"
        echo "[INFO] Contents of target after build "${CHECKPATH} && ls -l ${CHECKPATH}
        exit 1
    else
        echo "[INFO] The final name ${CHECKNAME} is correct"
    fi

fi
