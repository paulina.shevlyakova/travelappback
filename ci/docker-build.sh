#!/usr/bin/env bash
# Script to run the Docker build phase and putting the image to Docker repository
usage() {
	echo "Usage: $0"
	exit 1
}
# check no arguments are passed if so show usage
[[ $# -ne 0 ]] && usage
echo "*********************************************************************"
echo Project version = ${PROJECT_VERSION}
jar_name=${CI_PROJECT_NAME}-${PROJECT_VERSION}
echo Project target file name is ${jar_name}
echo "*********************************************************************"
echo [DEBUG] Checking cache

CACHE_FILE=/cache/${CI_PROJECT_PATH}/$CI_PROJECT_NAME:$CI_PIPELINE_ID/cache.zip
echo [DEBUG] Check cache exists ls -l ${CACHE_FILE} && ls -l ${CACHE_FILE}

echo [DEBUG] check contents of target directory ls -l `pwd`/target && ls -l `pwd`/target
echo "*********************************************************************"

#--no-cache=true
docker build --no-cache=true -t ${CI_PROJECT_NAME,,}:${PROJECT_VERSION,,} --build-arg jar_name=${jar_name} --build-arg http_proxy=${PROXY} --build-arg https_proxy=${PROXY} -f docker/Dockerfile .
