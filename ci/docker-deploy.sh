#!/usr/bin/env bash

set -e

NEXUS_REPO="index.docker.io/polinash"
CI_PROJECT_VERSION=$(echo ${CI_BUILD_REF_NAME,,} | sed -r 's/.*-([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}).*$/\1/')

PROJECT_NAME_SRC=${CI_PROJECT_NAME,,}:${PROJECT_VERSION,,}
PROJECT_NAME_DST=${NEXUS_REPO}/${CI_PROJECT_NAME,,}:${PROJECT_VERSION,,}
#docker pull ${PROJECT_NAME_DST} || ( echo 'Error! Try to restart previous stage!' ; exit 2 )


echo "Project version ${PROJECT_VERSION}"
echo "Branch version ${CI_PROJECT_VERSION}"
echo ${CI_COMMIT_REF_NAME,,}
if [ ${PROJECT_VERSION} != ${CI_PROJECT_VERSION} ]; then
  echo "Project and branch version mismatch."
  exit 3
fi

docker tag ${PROJECT_NAME_SRC} ${PROJECT_NAME_DST}
docker push ${PROJECT_NAME_DST}
[[ $? -ne 0 ]] && ( echo "DOCKER PUSH was FAILED" ; exit 1 )

echo "Docker image name: ${PROJECT_NAME_DST}"
