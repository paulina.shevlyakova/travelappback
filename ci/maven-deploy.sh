#!/usr/bin/env bash
# Script to run the Maven deploy phase and sending the artifacts to Nexus
usage() {
	echo "Usage: $0"
	exit 1
}
# check no arguments are passed if so show usage
[[ $# -ne 0 ]] && usage
# Invoke Maven deploy phase
#export PROJECT_VERSION="$(mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | sed -n -r  -e '/^[.*]/ !{ /^([0-9]{1,3}\.)*[0-9]{1,3}(-SNAPSHOT|$)$/ { p; q } }')"
echo Project version is ${PROJECT_VERSION}
mvn deploy || { echo "Maven Deploy failed" && false; }
