package ru.vsu.travel_app_back.integration;

import com.google.cloud.firestore.DocumentReference;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.model.DirectionsResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vsu.travel_app_back.WebAppContext;
import ru.vsu.travel_app_back.controller.TravelController;
import ru.vsu.travel_app_back.proxy.AiMatrixService;
import ru.vsu.travel_app_back.proxy.NewsService;
import ru.vsu.travel_app_back.proxy.SightsService;
import ru.vsu.travel_app_back.service.FirebaseInitialize;
import ru.vsu.travel_app_back.service.TravelService;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ContextConfiguration(classes = WebAppContext.class)
@WebMvcTest(controllers = TravelController.class)
public class TravelAppBackIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AiMatrixService aiMatrixService;

    @MockBean
    NewsService newsService;

    @MockBean
    SightsService sightsService;

    @MockBean
    FirebaseInitialize firebaseInitialize;

    @BeforeEach
    void setup() throws IOException {
        doNothing().when(firebaseInitialize).initialize();
    }

    @Test
    void calculatePathTest() throws Exception {
        when(aiMatrixService.calculateMatrix(anyString(), anyString(), anyString())).thenReturn(TestUtils.getDistanceMatrix());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/travelapp/calculate")
                .content(TestUtils.createInputEntity())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String resultSS = result.getResponse().getContentAsString();
        assertNotNull(resultSS);
    }

    @Test
    void calculatePathEmptyHomeTest() throws Exception {
        when(aiMatrixService.calculateMatrix(anyString(), anyString(), anyString())).thenReturn(TestUtils.getDistanceMatrix());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/travelapp/calculate")
                .content(TestUtils.createInputEntityWithEmptyHome())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    void calculatePathEmptyDaysTest() throws Exception {
        when(aiMatrixService.calculateMatrix(anyString(), anyString(), anyString())).thenReturn(TestUtils.getDistanceMatrix());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/travelapp/calculate")
                .content(TestUtils.createInputEntityWithEmptyDays())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void calculatePathEmptyCoordinatesTest() throws Exception {
        when(aiMatrixService.calculateMatrix(anyString(), anyString(), anyString())).thenReturn(TestUtils.getDistanceMatrix());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/travelapp/calculate")
                .content(TestUtils.createInputEntityWithEmptyCoordinates())
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

//    @Test
//    void saveRouteTest() throws Exception {
//        PowerMockito.stub(PowerMockito.method(DirectionsApiRequest.class, "await")).toReturn(new DirectionsResult());
//        PowerMockito.stub(PowerMockito.method(DocumentReference.class, "set", Object.class)).toReturn(null);
//
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/v1/travelapp/route")
//                .content(TestUtils.createRouteJson())
//                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andReturn();
//
//    }

//    @Test
//    void getRouteById() throws Exception {
//        PowerMockito.stub(PowerMockito.method(TravelService.class, "getRouteById")).toReturn(TestUtils.createRouteEntity());
//
//        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/travelapp/route/123456/123456"))
//                .andExpect(status().isOk())
//                .andReturn();
//        String resultSS = result.getResponse().getContentAsString();
//        assertNotNull(resultSS);
//    }

}
