package ru.vsu.travel_app_back.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.vsu.travel_app_back.domain.InputEntity;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.domain.Route;
import ru.vsu.travel_app_back.domain.distance_matrix.DistanceMatrixEntity;
import ru.vsu.travel_app_back.domain.distance_matrix.MatrixElement;
import ru.vsu.travel_app_back.domain.distance_matrix.MatrixRow;
import ru.vsu.travel_app_back.domain.distance_matrix.Value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestUtils {

    public static LatLong homePoint = new LatLong(55.749485, 37.640661);
    public static List<LatLong> coordinates = Arrays.asList(
            new LatLong(55.815150, 37.755358),
            new LatLong(55.801264, 37.799406),
            new LatLong(55.841701, 37.877834),
            new LatLong(55.601480, 37.644699),
            new LatLong(55.577563, 37.722400),
            new LatLong(55.749297, 37.503959),
            new LatLong(55.721473, 37.459910)
    );

    private static final Long[][] distanceMatrix = new Long[][]{
            {0L, 4L, 5L, 6L, 12L, 45L, 63L, 22L},
            {5L, 0L, 7L, 9L, 41L, 36L, 25L, 17L},
            {10L, 8L, 0L, 7L, 11L, 27L, 31L, 52L},
            {4L, 10L, 12L, 0L, 19L, 28L, 38L, 55L},
            {7L, 63L, 15L, 8L, 0L, 45L, 8L, 15L},
            {45L, 7L, 23L, 12L, 33L, 0L, 41L, 32L},
            {14L, 17L, 22L, 36L, 10L, 38L, 0L, 55L},
            {43L, 21L, 74L, 33L, 17L, 29L, 40L, 0L},
    };

    public static DistanceMatrixEntity getDistanceMatrix() {
        List<MatrixRow> rows = new ArrayList<>();
        for (int i = 0; i < distanceMatrix.length; i++) {
            MatrixRow row = new MatrixRow();
            List<MatrixElement> elements = new ArrayList<>();
            for (int j = 0; j < distanceMatrix.length; j++) {
                elements.add(new MatrixElement(new Value("", distanceMatrix[i][j]), null, null));
            }
            row.setElements(elements);
            rows.add(row);
        }
        return new DistanceMatrixEntity(null, null, rows, null);
    }

    public static String createInputEntity() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new InputEntity(1, homePoint, coordinates));
    }

    public static String createInputEntityWithEmptyHome() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new InputEntity(1, null, coordinates));
    }

    public static String createInputEntityWithEmptyDays() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new InputEntity(0, homePoint, coordinates));
    }

    public static String createInputEntityWithEmptyCoordinates() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new InputEntity(0, homePoint, new ArrayList<>()));
    }

    public static String createRouteJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(createRouteEntity());
    }

    public static Route createRouteEntity() throws JsonProcessingException {
        return new Route("123456", homePoint, "123456", "test", new ArrayList<>());
    }

}
