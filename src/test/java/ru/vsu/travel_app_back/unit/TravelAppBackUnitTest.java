package ru.vsu.travel_app_back.unit;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.errors.TravelException;
import ru.vsu.travel_app_back.service.clustering.Cluster;
import ru.vsu.travel_app_back.service.clustering.KMeans;
import ru.vsu.travel_app_back.service.graph.BranchAndBounds;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TravelAppBackUnitTest {

    @Test
    void geoDistanceNormalTest() throws TravelException {
        int i = 0;
        for (int j = 0; j < TestUtils.DistanceTestInput.size(); j++) {
            for (int k = j + 1; k < TestUtils.DistanceTestInput.size(); k++) {
                Double dist = BigDecimal
                        .valueOf(LatLong.distance(TestUtils.DistanceTestInput.get(j), TestUtils.DistanceTestInput.get(k)))
                        .setScale(2, BigDecimal.ROUND_HALF_UP)
                        .doubleValue();
                assertEquals(dist, TestUtils.DistanceTestEtalon.get(i++));

            }
        }
    }

    @Test
    void geoDistanceExceptionTest() throws TravelException {
        int i = 0;
        for (int j = 0; j < TestUtils.DistanceTestEmptyLatLongInput.size(); j++) {
            for (int k = j + 1; k < TestUtils.DistanceTestEmptyLatLongInput.size(); k++) {
                final int jIndex = j;
                final int kIndex = k;
                assertThrows(TravelException.class, () -> {
                    LatLong.distance(TestUtils.DistanceTestEmptyLatLongInput.get(jIndex), TestUtils.DistanceTestEmptyLatLongInput.get(kIndex));
                });

            }
        }
    }


    @Test
    void calculateClusterCenterTest() throws TravelException {
        for (int i = 1; i <= TestUtils.CalculateCenterTestInput.size(); i++) {
            List<LatLong> input = new ArrayList<>();
            int k = 1;
            while (k <= i) {
                input.addAll(TestUtils.CalculateCenterTestInput.get(k++));
            }
            LatLong center = KMeans.calculateCenter(input);
            assertEquals(center, TestUtils.CalculateCenterTestEtalon.get(i));
        }
    }

    @Test
    void calculateClusterCenterExceptionTest() throws TravelException {
        for (int i = 1; i <= TestUtils.CalculateCenterTestExceptionInput.size(); i++) {
            List<LatLong> input = new ArrayList<>();
            int k = 1;
            while (k <= i) {
                input.addAll(TestUtils.CalculateCenterTestExceptionInput.get(k++));
            }
            assertThrows(TravelException.class, () -> KMeans.calculateCenter(input));
        }
    }

    @Test
    void kmeansCalculateTest() throws TravelException {
        for (int i = 0; i < TestUtils.KMeansTestInput.size(); i++) {
            KMeans kMeans = new KMeans(TestUtils.KMeansTestInput.get(i).getDays(), TestUtils.KMeansTestInput.get(i).getCoordinates());
            List<Cluster> clusters = kMeans.calculate();
            clusters.sort((x, y) -> {
                if (x.getPoints().size() > y.getPoints().size())
                    return 1;
                else return 0;
            });

            for (int j = 0; j < clusters.size(); j++) {
                assertEquals(clusters.get(j).getPoints().size(), TestUtils.KMeansClustersEtalon.get(i + 1).get(j).getPoints().size());
            }
        }
    }

    @Test
    void kmeansCalculateExceptionTest() throws TravelException {
        assertThrows(TravelException.class, () -> new KMeans(0, TestUtils.KMeansTestInput.get(0).getCoordinates()));
        assertThrows(TravelException.class, () -> new KMeans(3, new ArrayList<>()));
    }

    @Test
    void addInfinityTest() {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        int i = 0;
        for (Long[][] matrix : TestUtils.MatrixInput) {
            branchAndBounds.addInfinity(matrix);
            assertTrue(TestUtils.assertMatrixEqual(matrix, TestUtils.AddInfinityEtalon.get(i++)));
        }

    }

    @Test
    void addInfinityEmptyTest() {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        Long[][] matrix = new Long[][]{};
        branchAndBounds.addInfinity(matrix);
        assertTrue(TestUtils.assertMatrixEqual(matrix, new Long[][]{}));
    }

    @Test
    void deleteRowColumnTest() throws TravelException {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        int i = 0;
        for (Long[][] matrix : TestUtils.MatrixInput) {
            Long[][] newMatrix = branchAndBounds.deleteRowAndColumn(matrix, TestUtils.deletedIndex.get(i).getLeft(), TestUtils.deletedIndex.get(i).getRight());
            assertTrue(TestUtils.assertMatrixEqual(newMatrix, TestUtils.DeleteRowColumnEtalon.get(i++)));
        }
    }

    @Test
    void deleteRowColumnExceptionTest() throws TravelException {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        int i = 0;
        for (Long[][] matrix : TestUtils.MatrixInput) {
            assertThrows(TravelException.class, () -> branchAndBounds.deleteRowAndColumn(matrix, TestUtils.deletedExceptionIndex.get(i).getLeft(), TestUtils.deletedExceptionIndex.get(i).getRight()));
        }
    }

    @Test
    void getMaxCoefficientTest() throws TravelException {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        int i = 0;
        for (Long[][] matrix : TestUtils.GetMaxCoefficientInput) {
            Pair<Integer, Integer> result = branchAndBounds.getMaxCoefficient(matrix);
            assertEquals(result.getLeft(), TestUtils.GetMaxCoefficientEtalon.get(i).getLeft());
            assertEquals(result.getRight(), TestUtils.GetMaxCoefficientEtalon.get(i++).getRight());
        }
    }

    @Test
    void branchAndBoundsTest() throws TravelException {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        ArrayList<LatLong> points = new ArrayList<>(TestUtils.KMeansCoordinates.subList(0, 5));
        Map<LatLong, LatLong> path = branchAndBounds.apply(TestUtils.DistanceMatrix, new HashMap<>(), points, new ArrayList<>(points));
        assertTrue(TestUtils.assertMapEqual(path, TestUtils.BranchBoundsEtalon));
    }

    @Test
    void branchAndBoundsExceptionTest() throws TravelException {
        BranchAndBounds branchAndBounds = new BranchAndBounds();
        assertThrows(TravelException.class, () -> branchAndBounds.apply(TestUtils.DistanceMatrix, new HashMap<>(), new ArrayList<>(), new ArrayList<>()));
    }

}
