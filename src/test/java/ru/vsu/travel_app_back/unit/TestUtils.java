package ru.vsu.travel_app_back.unit;

import org.apache.commons.lang3.tuple.Pair;
import ru.vsu.travel_app_back.domain.InputEntity;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.service.clustering.Cluster;

import java.util.*;
import java.util.Map.Entry;

public class TestUtils {
//    private static String ResourcesPath = String.join(File.separator, "src", "test", "resources");
//    public static String DistanceInputFile = String.join(File.separator, ResourcesPath, "distance_input.txt");
//    public static String DistanceEtalonFile = String.join(File.separator, ResourcesPath, "distance_etalon.txt");
//
//
//    public static List<String> readInputList(String path) throws IOException {
//        BufferedReader br = new BufferedReader(new FileReader(path));
//        List<String> result = new ArrayList<>();
//        String line = "";
//
//        while ((line = br.readLine()) != null) {
//            result.add(line);
//        }
//        return result;
//    }

    public static List<LatLong> DistanceTestInput = Arrays.asList(
            new LatLong(51.730281, 39.154828),
            new LatLong(52.606659, 39.603520),
            new LatLong(54.616856, 39.736603),
            new LatLong(55.732221, 37.677279));
    public static List<Double> DistanceTestEtalon = Arrays.asList(102.14, 323.3, 455.46, 223.7, 369.44, 180.21);
    public static List<LatLong> DistanceTestEmptyLatLongInput = Arrays.asList(
            new LatLong(0.0, 0.0),
            new LatLong(52.606659, null),
            new LatLong(null, null),
            null);


    public static Map<Integer, List<LatLong>> CalculateCenterTestInput = new HashMap<Integer, List<LatLong>>() {
        {
            put(1, Arrays.asList(new LatLong(51.730281, 39.154828), new LatLong(52.606659, 39.603520)));
            put(2, Collections.singletonList(new LatLong(54.616856, 39.736603)));
            put(3, Collections.singletonList(new LatLong(55.732221, 37.677279)));
        }
    };
    public static Map<Integer, LatLong> CalculateCenterTestEtalon = new HashMap<Integer, LatLong>() {
        {
            put(1, new LatLong(52.16868275117596, 39.37696450961947));
            put(2, new LatLong(52.98482098754406, 39.492292398901));
            put(3, new LatLong(53.674174782907684, 39.06088223253234));
        }
    };
    public static Map<Integer, List<LatLong>> CalculateCenterTestExceptionInput = new HashMap<Integer, List<LatLong>>() {
        {
            put(1, Arrays.asList(new LatLong(51.730281, null), new LatLong(52.606659, 39.603520)));
            put(2, Collections.singletonList(new LatLong(0.0, 0.0)));
            put(3, Collections.singletonList(null));
        }
    };

    public static List<LatLong> KMeansCoordinates = Arrays.asList(
            new LatLong(55.815150, 37.755358),
            new LatLong(55.801264, 37.799406),
            new LatLong(55.841701, 37.877834),
            new LatLong(55.601480, 37.644699),
            new LatLong(55.577563, 37.722400),
            new LatLong(55.749297, 37.503959),
            new LatLong(55.721473, 37.459910)
    );

    public static List<InputEntity> KMeansTestInput = Arrays.asList(
            new InputEntity(1, null, KMeansCoordinates),
            new InputEntity(2, null, KMeansCoordinates),
            new InputEntity(3, null, KMeansCoordinates)
    );

    public static Map<Integer, List<Cluster>> KMeansClustersEtalon = new HashMap<Integer, List<Cluster>>() {{
        put(1, Collections.singletonList(createCluster(KMeansCoordinates)));
        put(2, Arrays.asList(createCluster(KMeansCoordinates.subList(5, KMeansCoordinates.size())), createCluster(KMeansCoordinates.subList(0, 5))));
        put(3, Arrays.asList(createCluster(KMeansCoordinates.subList(3, 5)), createCluster(KMeansCoordinates.subList(5, KMeansCoordinates.size())), createCluster(KMeansCoordinates.subList(0, 3))));
    }};

    public static List<Long[][]> MatrixInput = Arrays.asList(
            new Long[][]{{Long.MAX_VALUE, 1L, 5L},
                    {5L, Long.MAX_VALUE, 7L},
                    {10L, 8L, 0L}},
            new Long[][]{{Long.MAX_VALUE, 4L, 5L, 6L},
                    {5L, Long.MAX_VALUE, 7L, 9L},
                    {10L, 8L, 4L, 7L},
                    {4L, 10L, 12L, Long.MAX_VALUE}}
    );
    public static List<Long[][]> AddInfinityEtalon = Arrays.asList(
            new Long[][]{{Long.MAX_VALUE, 1L, 5L},
                    {5L, Long.MAX_VALUE, 7L},
                    {10L, 8L, Long.MAX_VALUE}},
            new Long[][]{{Long.MAX_VALUE, 4L, 5L, 6L},
                    {5L, Long.MAX_VALUE, 7L, 9L},
                    {10L, 8L, Long.MAX_VALUE, 7L},
                    {4L, 10L, 12L, Long.MAX_VALUE}}
    );

    public static List<Pair<Integer, Integer>> deletedIndex = Arrays.asList(
            Pair.of(2, 2),
            Pair.of(0, 2)
    );
    public static List<Pair<Integer, Integer>> deletedExceptionIndex = Arrays.asList(
            Pair.of(2, 5),
            Pair.of(5, 5)
    );
    public static List<Long[][]> DeleteRowColumnEtalon = Arrays.asList(
            new Long[][]{{Long.MAX_VALUE, 1L}, //2,2
                    {5L, Long.MAX_VALUE}},
            new Long[][]{{5L, Long.MAX_VALUE, 9L}, //0,2
                    {10L, 8L, 7L},
                    {4L, 10L, Long.MAX_VALUE}}
    );

    public static List<Long[][]> GetMaxCoefficientInput = Arrays.asList(
            new Long[][]{{Long.MAX_VALUE, 0L, 5L},
                    {0L, Long.MAX_VALUE, 9L},
                    {10L, 8L, 0L}},
            new Long[][]{{Long.MAX_VALUE, 4L, 9L, 0L},
                    {5L, Long.MAX_VALUE, 0L, 9L},
                    {0L, 8L, 4L, 7L},
                    {4L, 0L, 6L, Long.MAX_VALUE}},
            new Long[][]{{Long.MAX_VALUE, 4L, 5L, 6L, 0L},
                    {5L, Long.MAX_VALUE, 0L, 9L, 7L},
                    {10L, 0L, Long.MAX_VALUE, 0L, 12L},
                    {0L, 10L, 12L, Long.MAX_VALUE, 11L},
                    {4L, 10L, 0L, 21L, Long.MAX_VALUE}}
    );
    public static List<Pair<Integer, Integer>> GetMaxCoefficientEtalon = Arrays.asList(
            Pair.of(1, 0),
            Pair.of(0, 3),
            Pair.of(3, 0)
    );

    public static Long[][] DistanceMatrix = new Long[][]{
            {Long.MAX_VALUE, 4L, 5L, 6L},
            {5L, Long.MAX_VALUE, 7L, 9L},
            {10L, 8L, Long.MAX_VALUE, 7L},
            {4L, 10L, 12L, Long.MAX_VALUE}};

    public static Map<LatLong, LatLong> BranchBoundsEtalon = new HashMap<LatLong, LatLong>() {{
        put(new LatLong(55.601480, 37.644699), new LatLong(55.815150, 37.755358));
        put(new LatLong(55.841701, 37.877834), new LatLong(55.601480, 37.644699));
        put(new LatLong(55.815150, 37.755358), new LatLong(55.801264, 37.799406));
        put(new LatLong(55.801264, 37.799406), new LatLong(55.841701, 37.877834));
    }};

    public static boolean assertMatrixEqual(Long[][] matrix1, Long[][] matrix2) {
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                if (!matrix1[i][j].equals(matrix2[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean assertMapEqual(Map<LatLong, LatLong> map1, Map<LatLong, LatLong> map2) {
        if (map1.size() != map2.size()) {
            return false;
        }
        for (Entry<LatLong, LatLong> entry : map1.entrySet()) {
            LatLong value = map2.get(entry.getKey());
            if (value == null || !value.equals(entry.getValue())) {
                return false;
            }
        }
        return true;
    }

    private static Cluster createCluster(List<LatLong> points) {
        Cluster cluster = new Cluster();
        cluster.setPoints(new ArrayList<>(points));
        return cluster;
    }
}
