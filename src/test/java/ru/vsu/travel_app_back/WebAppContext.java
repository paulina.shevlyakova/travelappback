package ru.vsu.travel_app_back;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.vsu.travel_app_back.controller.TravelController;
import ru.vsu.travel_app_back.errors.TravelExceptionHandler;
import ru.vsu.travel_app_back.service.FirebaseInitialize;
import ru.vsu.travel_app_back.service.TravelService;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = {TravelController.class, TravelService.class, TravelExceptionHandler.class})
public class WebAppContext extends WebMvcConfigurerAdapter {

}
