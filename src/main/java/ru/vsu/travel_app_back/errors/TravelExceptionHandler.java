package ru.vsu.travel_app_back.errors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class TravelExceptionHandler {

    @ExceptionHandler(value = TravelException.class)
    public ResponseEntity<ErrorMessage> catchUserErrors(TravelException e) {
        return new ResponseEntity<>(new ErrorMessage(e.getMessage()), e.getStatus());
    }
}
