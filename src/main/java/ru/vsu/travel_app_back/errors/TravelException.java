package ru.vsu.travel_app_back.errors;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class TravelException extends Exception {
    private String message;
    private HttpStatus status;

    public TravelException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
