package ru.vsu.travel_app_back.errors;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import feign.Logger;
import feign.codec.ErrorDecoder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import java.io.IOException;

//import org.apache.commons.lang.StringUtils;

@Configuration
public class FeignPropagateBadRequestsConfiguration {
//    @Bean
//    public Logger.Level feignLoggerLevel() {
//        return Logger.Level.FULL;
//    }

//    @Bean
//    public RequestInterceptor requestInterceptor() {
//        return requestTemplate -> {
//            requestTemplate.header("Content-Type", "application/json");
//            requestTemplate.header("Accept", "application/json");
//            requestTemplate.header("x-rapidapi-host", "trueway-matrix.p.rapidapi.com");
//            requestTemplate.header("x-rapidapi-key", "cedb6b94c3mshd0b7cee3438281ep11f877jsn7e93a6f7b076");
//        };
//    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return (methodKey, response) -> {
            int status = response.status();
            if (status == 400) {
                String body = "Bad request";
                try {
                    body = IOUtils.toString(response.body().asReader());
                } catch (IOException ignored) {
                }
                HttpHeaders httpHeaders = new HttpHeaders();
                response.headers().forEach((k, v) -> httpHeaders.add("feign-" + k, StringUtils.join(v, ",")));
                return new FeignBadResponseWrapper(status, httpHeaders, body);
            } else {
                return new RuntimeException("Response Code " + status);
            }
        };
    }

    @Getter
    @Setter
    public class FeignBadResponseWrapper extends HystrixBadRequestException {
        private final int status;
        private final HttpHeaders headers;
        private final String body;

        public FeignBadResponseWrapper(int status, HttpHeaders headers, String body) {
            super("Bad request");
            this.status = status;
            this.headers = headers;
            this.body = body;
        }
    }
}
