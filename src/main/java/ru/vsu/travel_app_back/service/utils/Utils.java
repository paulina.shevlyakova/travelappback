package ru.vsu.travel_app_back.service.utils;

import org.springframework.http.HttpStatus;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.errors.TravelException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {

    public static Map<String, String> objectCategories = new HashMap<String, String>() {{
        put("amusement_parks", "Парки развлечений");
        put("palaces", "Дворцы");
        put("amphitheatres", "Амфитеатры");
        put("pyramids", "Пирамиды");
        put("museums", "Музеи");
        put("fountains", "Фонтаны");
        put("gardens_and_parks", "Сады и парки");
        put("sculptures", "Памятники");
        put("squares", "Площади");
        put("wall_painting", "Граффити");
        put("beaches", "Пляжи");
        put("islands", "Острова");
        put("nature_reserves", "Природные заповедники");
        put("natural_springs", "Природные источники");
        put("water", "Водные богатства");
        put("view_points", "Смотровые площадки");
        put("cathedrals", "Соборы и церкви");
    }};

    public static double degreesToRadians(double degrees) {
        return (degrees * Math.PI) / 180;

    }

    public static double radiansToDegrees(double radians) {
        return (radians * 180) / Math.PI;

    }

    public static String coordListToStr(ArrayList<LatLong> coord, String delimiter) {
        if (coord.size() > 0) {
            StringBuilder result = new StringBuilder(coord.get(0).toString());
            for (LatLong latLong : coord.subList(1, coord.size())) {
                result.append(delimiter)
                        .append(latLong.toString());
            }
            return result.toString();
        } else {
            return "";
        }
    }

    public static String listToStr(List<String> list, String delimiter) {
        if (list.size() > 0) {
            StringBuilder result = new StringBuilder(list.get(0));
            for (String str : list.subList(1, list.size())) {
                result.append(delimiter)
                        .append(str);
            }
            return result.toString();
        } else {
            return "";
        }
    }
    public static void checkGeoPoint(LatLong point) throws TravelException {
        if (point == null || point.getLat() == null || point.getLng() == null
                || (point.getLng() == 0 && point.getLat() == 0)) {
            throw new TravelException(HttpStatus.BAD_REQUEST, "Координаты гео-точки не заданы");
        }

    }
}
