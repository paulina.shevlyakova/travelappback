package ru.vsu.travel_app_back.service.graph;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.HttpStatus;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.errors.TravelException;

import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BranchAndBounds {
    private Long inf = Long.MAX_VALUE;
    private Long minBound = 0L;

    public Map<LatLong, LatLong> apply(Long[][] matrix,
                                       Map<LatLong, LatLong> path,
                                       ArrayList<LatLong> rowCoordinates,
                                       ArrayList<LatLong> columnCoordinates) throws TravelException {
        if (matrix.length == 0 || rowCoordinates.size() == 0 || columnCoordinates.size() == 0) {
            throw new TravelException(HttpStatus.BAD_REQUEST, "Матрица расстояний пуста, либо пусты соответстсвующие списки вершин");
        }
        if (matrix.length > 2) {
            Long minRowValue;
            Long[] minColumnValue = new Long[matrix.length];
            Arrays.fill(minColumnValue, inf);

            //ищем минимальные элементы в строках и столбцах
            for (int i = 0; i < matrix.length; i++) {
                minRowValue = inf;
                for (int j = 0; j < matrix.length; j++) {
                    if (minRowValue > matrix[i][j]) {
                        minRowValue = matrix[i][j];
                    }
                }

                //вычитаем минимальное значение из всех элементов строки
                for (int j = 0; j < matrix.length; j++) {
                    if (!matrix[i][j].equals(inf)) {
                        matrix[i][j] -= minRowValue;
                        if (minColumnValue[j] > matrix[i][j]) {
                            minColumnValue[j] = matrix[i][j];
                        }
                    }
                }
                //увеличиваем границу
                minBound += minRowValue;
            }

            //вычитаем минимальное значение из всех элементов столбца
            for (int j = 0; j < matrix.length; j++) {
                for (int i = 0; i < matrix.length; i++) {
                    if (!matrix[i][j].equals(inf)) {
                        matrix[i][j] -= minColumnValue[j];
                    }
                }
                //увеличиваем границу
                minBound += minColumnValue[j];
            }

            //коррдинаты элемента с максимальным коэффициентом
            Pair<Integer, Integer> zeroPoint = getMaxCoefficient(matrix);
            path.put(rowCoordinates.get(zeroPoint.getKey()), columnCoordinates.get(zeroPoint.getValue()));

            rowCoordinates.remove((int) zeroPoint.getKey());
            columnCoordinates.remove((int) zeroPoint.getValue());

            Long[][] newMatrix = deleteRowAndColumn(matrix.clone(), zeroPoint.getKey(), zeroPoint.getValue());

            addInfinity(newMatrix);

            if (newMatrix.length > 2) {
                apply(newMatrix, path, rowCoordinates, columnCoordinates);
            } else {
                for (int i = 0; i < newMatrix.length; i++) {
                    for (int j = 0; j < newMatrix.length; j++) {
                        if (!Objects.equals(newMatrix[i][j], inf)) {
                            path.put(rowCoordinates.get(i), columnCoordinates.get(j));
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    if (!Objects.equals(matrix[i][j], inf)) {
                        path.put(rowCoordinates.get(i), columnCoordinates.get(j));
                    }
                }
            }
        }
        return path;
    }

    /**
     * Вычисление коэффициентов для нулевых элементов матрицы
     *
     * @param matrix  - матрица расстояний
     * @param zeroRow - индекс строки нулевого элемента
     * @param zeroCol - индекс столбца нулевого элемента
     * @return рассчитанный коэффициент
     */
    public Long getCoefficient(Long[][] matrix, int zeroRow, int zeroCol) {
        Long minColValue = inf, minRowValue = inf;
        for (int i = 0; i < matrix.length; i++) {
            if (i != zeroRow) {
                minRowValue = Math.min(minRowValue, matrix[i][zeroCol]);
            }
            if (i != zeroCol) {
                minColValue = Math.min(minColValue, matrix[zeroRow][i]);
            }
        }
        return minColValue + minRowValue;
    }

    /**
     * Получение индексов элементов с максимальным коэффициентом
     *
     * @param matrix - матрица рассточний
     * @return список индексов в виде List((row,column))
     */
    public Pair<Integer, Integer> getMaxCoefficient(Long[][] matrix) {
        Map<Long, Pair<Integer, Integer>> zeroCoordinates = new HashMap<>();
        Long maxCoeff = 0L;

        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i][j] == 0) {
                    Long coeff = getCoefficient(matrix, i, j);
                    zeroCoordinates.put(coeff, Pair.of(i, j));
                    maxCoeff = Math.max(maxCoeff, coeff);
                }
            }
        }
        return zeroCoordinates.get(maxCoeff);
    }

    public Long[][] deleteRowAndColumn(Long[][] matrix, int row, int column) throws TravelException {
        if (row >= matrix.length || column >= matrix.length) {
            throw new TravelException(HttpStatus.BAD_REQUEST, "Номер строки или столбца для удаления больше размера матрицы");
        }

        Long[][] result = new Long[matrix.length - 1][matrix.length - 1];
        int c, r;

        for (int i = 0; i < result.length; i++) {
            if (i >= row) {
                r = i + 1;
            } else {
                r = i;
            }
            for (int j = 0; j < result.length; j++) {
                if (j >= column) {
                    c = j + 1;
                } else {
                    c = j;
                }
                result[i][j] = matrix[r][c];
            }
        }
        return result;
    }

    public void addInfinity(Long[][] matrix) {
        Map<Integer, Boolean> rowInf = new HashMap<Integer, Boolean>() {{
            for (int i = 0; i < matrix.length; i++) {
                put(i, false);
            }
        }};
        Map<Integer, Boolean> colInf = new HashMap<Integer, Boolean>() {{
            for (int i = 0; i < matrix.length; i++) {
                put(i, false);
            }
        }};

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (Objects.equals(matrix[i][j], inf)) {
                    rowInf.put(i, true);
                    colInf.put(j, true);
                }
            }
        }


        List<Integer> rowRes = rowInf.entrySet().stream()
                .filter(x -> x.getValue().equals(false))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        int row = rowRes.size() > 0 ? rowRes.get(0) : -1;

        List<Integer> colRes = colInf.entrySet().stream()
                .filter(x -> x.getValue().equals(false))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        int col = colRes.size() > 0 ? colRes.get(0) : -1;

        if (row > -1 && col > -1) {
            matrix[row][col] = inf;
        }
    }
}
