package ru.vsu.travel_app_back.service.clustering;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.errors.TravelException;
import ru.vsu.travel_app_back.service.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KMeans {

    private List<String> colours = Arrays.asList("red", "purple", "orange", "blue",
            "beige", "darkgreen", "lightgreen", "darkblue", "lightblue", "lightred",
            "darkpurple", "pink", "cadetblue", "white", "gray", "lightgray", "black", "green");

    private int ClustersCnt;

    private List<LatLong> points;
    private List<Cluster> clusters = new ArrayList<Cluster>();

    public KMeans(int clusterCnt, List<LatLong> points) throws TravelException {
        if (clusterCnt <= 0 || points.size() == 0) {
            throw new TravelException(HttpStatus.BAD_REQUEST, "Не указано количество дней или не заданы точки");
        }
        this.ClustersCnt = clusterCnt;
        this.points = points;

        for (int i = 0; i < ClustersCnt; i++) {
            Cluster cluster = new Cluster();
            LatLong centroid = new LatLong(points.get(i));
            cluster.setCenter(centroid);
            cluster.setColor(colours.get(i));
            cluster.setId(i + 1);
            clusters.add(cluster);
        }
    }

    private void plotClusters() {
        for (int i = 0; i < ClustersCnt; i++) {
            Cluster c = clusters.get(i);
//            c.plotCluster();
        }
    }

    public List<Cluster> calculate() throws TravelException {
        boolean finish = false;
        int iteration = 30;

        while (!finish && iteration > 0) {
            clearClusters();

            List<LatLong> lastCentroids = getCentroids();
            assignCluster();
            calculateCentroids();

            iteration--;

            List<LatLong> currentCentroids = getCentroids();

            //Calculates total distance between new and old Centroids
            double distance = 0;
            for (int i = 0; i < lastCentroids.size(); i++) {
                distance += LatLong.distance(lastCentroids.get(i), currentCentroids.get(i));
            }
//            log.debug("Iteration: " + iteration);
//            log.debug("Centroid distances: " + distance);
//            plotClusters();

            if (distance == 0) {
                finish = true;
            }
        }
        return this.clusters;
    }

    private void clearClusters() {
        for (Cluster cluster : clusters) {
            cluster.clear();
        }
    }

    private List<LatLong> getCentroids() {
        List<LatLong> centroids = new ArrayList<LatLong>(ClustersCnt);
        for (Cluster cluster : clusters) {
            LatLong cntr = cluster.getCenter();
            centroids.add(cntr);
        }
        return centroids;
    }

    private void assignCluster() throws TravelException {
        double max = Double.MAX_VALUE;
        double min;
        int cluster = 0;
        double distance = 0.0;

        for (LatLong point : points) {
            min = max;
            for (int i = 0; i < ClustersCnt; i++) {
                Cluster c = clusters.get(i);
                distance = LatLong.distance(point, c.getCenter());
                if (distance < min) {
                    min = distance;
                    cluster = i;
                }
            }
            clusters.get(cluster).addPoint(point);
        }
    }

    private void calculateCentroids() throws TravelException {
        for (Cluster cluster : clusters) {
            LatLong newCenter = calculateCenter(cluster.getPoints());
            cluster.setCenter(newCenter);
        }
    }

    public static LatLong calculateCenter(List<LatLong> clusterPoints) throws TravelException {

        for (LatLong point : clusterPoints) {
            Utils.checkGeoPoint(point);
        }

        double x = 0;
        double y = 0;
        double z = 0;
        for (LatLong point : clusterPoints) {
            double latRadians = Utils.degreesToRadians(point.getLat());
            double lonRadians = Utils.degreesToRadians(point.getLng());

            x += Math.cos(latRadians) * Math.cos(lonRadians);
            y += Math.cos(latRadians) * Math.sin(lonRadians);
            z += Math.sin(latRadians);
        }
        int pointsCnt = clusterPoints.size();

        x = x / pointsCnt;
        y = y / pointsCnt;
        z = z / pointsCnt;

        double centralLon = Math.atan2(y, x);
        double centralSqrt = Math.sqrt(x * x + y * y);
        double centralLat = Math.atan2(z, centralSqrt);

        return new LatLong(Utils.radiansToDegrees(centralLat), Utils.radiansToDegrees(centralLon));
    }
}
