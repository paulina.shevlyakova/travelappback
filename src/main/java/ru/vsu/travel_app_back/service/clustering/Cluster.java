package ru.vsu.travel_app_back.service.clustering;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import ru.vsu.travel_app_back.domain.LatLong;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
@AllArgsConstructor
public class Cluster {
    private ArrayList<LatLong> points;
    private LatLong center;
    private int id;
    private String color;
    private List<String> drivingSteps;
    private List<String> walkingSteps;
    private List<String> transitSteps;

    public Cluster() {
        points = new ArrayList<>();
        drivingSteps = new ArrayList<>();
        walkingSteps = new ArrayList<>();
        transitSteps = new ArrayList<>();
        center = new LatLong();
    }

    public void plotCluster() {
        log.debug("[Cluster: " + id + "]");
        log.debug("[Centroid: " + center + "]");
        log.debug("[Points: \n");
        for (LatLong p : points) {
            log.debug(p.toString());
        }
        log.debug("]");
    }

    public void addPoint(LatLong point) {
        this.points.add(point);
    }

    public void clear() {
        points.clear();
    }

}
