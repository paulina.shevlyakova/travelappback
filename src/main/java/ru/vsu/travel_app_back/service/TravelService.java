package ru.vsu.travel_app_back.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.TravelMode;
import com.google.maps.model.VehicleType;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.vsu.travel_app_back.domain.InputEntity;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.domain.Route;
import ru.vsu.travel_app_back.domain.distance_matrix.DistanceMatrixEntity;
import ru.vsu.travel_app_back.domain.distance_matrix.MatrixElement;
import ru.vsu.travel_app_back.domain.distance_matrix.MatrixRow;
import ru.vsu.travel_app_back.domain.news.Document;
import ru.vsu.travel_app_back.domain.news.NewsEntity;
import ru.vsu.travel_app_back.domain.sights.*;
import ru.vsu.travel_app_back.errors.TravelException;
import ru.vsu.travel_app_back.proxy.AiMatrixService;
import ru.vsu.travel_app_back.proxy.NewsService;
import ru.vsu.travel_app_back.proxy.SightsService;
import ru.vsu.travel_app_back.service.clustering.Cluster;
import ru.vsu.travel_app_back.service.clustering.KMeans;
import ru.vsu.travel_app_back.service.graph.BranchAndBounds;
import ru.vsu.travel_app_back.service.utils.Utils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TravelService {
    private final AiMatrixService aiMatrixService;
    private final NewsService newsService;
    private final SightsService sightsService;

    @Value("${google.api.key}")
    private String googleKey;

    @Value("${nyt.api.key}")
    private String newsKey;

    @Value("${sights.api.key}")
    private String sightsKey;

    @Value("${ai.api.key}")
    private String aiKey;

    private final BranchAndBounds branchAndBounds = new BranchAndBounds();

    private final String baseImageUrl = "https://static01.nyt.com/";

    public Route analyzeData(InputEntity input) throws TravelException {
        checkEntity(input);

        return new Route("",
                input.getHome(),
                getRouteId(),
                "",
                calculateOptimRoute(input.getHome(),
                        makeClusters(input)));
    }

    public void saveRoute(Route route) throws InterruptedException, ApiException, IOException, ExecutionException {

        getSteps(route);
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore
                .collection("route")
                .document(route.getUserId())
                .collection(route.getRouteId())
                .document(route.getName())
                .set(route);
    }

    public void deleteRoute(String userId, String routeId) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> writeResult = dbFirestore.collection("route")
                .document(userId)
                .collection(routeId)
                .document()
                .delete();
    }

    public List<Route> getRoutes(String userId) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        Iterable<CollectionReference> collectionReferences = dbFirestore.collection("route")
                .document(userId)
                .listCollections();

        List<Route> routes = new ArrayList<>();
        collectionReferences.forEach(x -> {
            Iterable<DocumentReference> documentReferences = x.listDocuments();
            documentReferences.forEach(doc -> {
                try {
                    DocumentSnapshot document = doc.get().get();
                    if (document.exists()) {
                        routes.add(document.toObject(Route.class));
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

            });

        });
        return routes;
    }

    public Route getRouteById(String userId, String routeId) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference collectionReference = dbFirestore.collection("route")
                .document(userId)
                .collection(routeId);

        Route route = null;
        Iterator<DocumentReference> documentReferences = collectionReference.listDocuments().iterator();
        try {
            if (documentReferences.hasNext()) {
                DocumentSnapshot document = documentReferences.next().get().get();
                if (document.exists()) {
                    route = document.toObject(Route.class);
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return route;
    }

    public List<Document> getNews(int page) {

        NewsEntity news = newsService.getArticles("travel", newsKey, page);
        List<Document> docs = news.getResponse().getDocs();
        return docs.stream()
                .filter(x -> x.getMultimedia().size() > 0)
                .peek(x -> x.setImage_url(baseImageUrl + x.getMultimedia().get(0).getUrl()))
                .collect(Collectors.toList());
    }

    public LatLong getCityCoordinatesByName(CityEntity city) {
        CityAnswerEntity answ = sightsService.getCityCoordinates(city.getName(), sightsKey);
        return new LatLong(answ.getLat(), answ.getLon());
    }

    public List<SightAnswerEntity> getCityObjects(LatLong cityCoordinate) {
        Double radius = 10000.0;
        SightEntity answ = sightsService.getCityObjects(radius.toString(),
                cityCoordinate.getLng().toString(),
                cityCoordinate.getLat().toString(),
                Utils.listToStr(new ArrayList<>(Utils.objectCategories.keySet()), ","),
                sightsKey);
        List<SightAnswerEntity> result = new ArrayList<>();
        for (FeatureEntity feature : answ.getFeatures()) {
            Double lon = feature.getGeometry().getCoordinates().get(0);
            Double lat = feature.getGeometry().getCoordinates().get(1);

            String name = feature.getProperties().getName();

            String category = "";
            for (String str : feature.getProperties().getKinds().split(",")) {
                category = Utils.objectCategories.getOrDefault(str, null);
                if (category != null) {
                    break;
                }
            }

            if (name != null && !name.isEmpty()) {
                result.add(new SightAnswerEntity(name, new LatLong(lat, lon), category));
            }
        }
        return result;
    }

    private void checkEntity(InputEntity input) throws TravelException {
        if (input.getDays() <= 0 || input.getCoordinates().size() == 0) {
            throw new TravelException(HttpStatus.BAD_REQUEST, "Не задана домашняя точка или координаты мест посещения");
        }
        Utils.checkGeoPoint(input.getHome());
        for (LatLong point : input.getCoordinates()) {
            Utils.checkGeoPoint(point);
        }
    }

    private void getSteps(Route route) throws InterruptedException, ApiException, IOException {
        for (Cluster cluster : route.getClusters()) {
            DirectionsResult result = null;
            List<String> transitSteps = new ArrayList<>();
            for (int i = 0; i < cluster.getPoints().size() - 1; i++) {
                result = getTransitDirection(cluster.getPoints().get(i), cluster.getPoints().get(i + 1));
                transitSteps.addAll(convertDirectionResToSteps(result));
            }
            cluster.setTransitSteps(transitSteps);
            cluster.setDrivingSteps(convertDirectionResToSteps(getNontransitDirection(route.getHome(), cluster.getPoints(), TravelMode.DRIVING)));
            cluster.setWalkingSteps(convertDirectionResToSteps(getNontransitDirection(route.getHome(), cluster.getPoints(), TravelMode.WALKING)));
        }
    }

    private List<Cluster> calculateOptimRoute(LatLong home,
                                              List<Cluster> clusters) throws TravelException {

        for (Cluster cluster : clusters) {

            cluster.addPoint(home);
            DistanceMatrixEntity res = aiMatrixService.calculateMatrix(
                    Utils.coordListToStr(cluster.getPoints(), "|"),
                    Utils.coordListToStr(cluster.getPoints(), "|"),
                    aiKey);

            Long[][] matrix = new Long[cluster.getPoints().size()][cluster.getPoints().size()];

            int i = 0;
            for (MatrixRow row : res.getRows()) {
                int j = 0;
                for (MatrixElement element : row.getElements()) {
                    if (i == j) {
                        matrix[i][j++] = Long.MAX_VALUE;
                    } else {
                        matrix[i][j++] = element.getDistance().getValue();
                    }
                }
                i++;
            }
            Map<LatLong, LatLong> path = branchAndBounds.apply(matrix, new HashMap<>(),
                    new ArrayList<>(cluster.getPoints()),
                    new ArrayList<>(cluster.getPoints()));
            ArrayList<LatLong> orderedList = new ArrayList<>();

            i = 1;
            orderedList.add(home);
            LatLong next = path.get(home);
            orderedList.add(new LatLong(next));
            orderedList.get(orderedList.size() - 1).setNumber(i++);

            while (orderedList.size() < path.size()) {
                next = path.get(next);
                orderedList.add(new LatLong(next));
                orderedList.get(orderedList.size() - 1).setNumber(i++);
            }

            cluster.setPoints(orderedList);
        }
        return clusters;

    }

    private List<Cluster> makeClusters(InputEntity input) throws TravelException {
        KMeans kMeans = new KMeans(input.getDays(), input.getCoordinates());
        return kMeans.calculate();
    }

    private String getRouteId() {
        return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
    }

    private DirectionsResult getNontransitDirection(LatLong home, List<LatLong> coordinates, TravelMode mode) throws InterruptedException, ApiException, IOException {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(googleKey)
                .build();

        return DirectionsApi
                .newRequest(context)
                .mode(mode)
                .language("ru")
                .origin(home.toString())
                .destination(home.toString())
                .waypoints(Utils.coordListToStr(new ArrayList<>(coordinates.subList(1, coordinates.size())), "|"))
                .await();

    }

    private DirectionsResult getTransitDirection(LatLong origin, LatLong dest) throws InterruptedException, ApiException, IOException {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(googleKey)
                .build();

        return DirectionsApi
                .newRequest(context)
                .mode(TravelMode.TRANSIT)
                .language("ru")
                .origin(origin.toString())
                .destination(dest.toString())
                .await();

    }

    private List<String> convertDirectionResToSteps(DirectionsResult dirResult) {
        List<String> res = new ArrayList<>();
        for (DirectionsStep step : dirResult.routes[0].legs[0].steps) {
            StringBuilder sb = new StringBuilder();

            sb.append(step.htmlInstructions);
            sb.append("<br>");

            if (step.travelMode.equals(TravelMode.WALKING)) {
                if (step.steps != null) {
                    for (DirectionsStep innerStep : step.steps) {
                        if (innerStep.htmlInstructions != null) {
                            sb.append(innerStep.htmlInstructions);
                            sb.append("<br>");
                        }
                    }
                }
            } else if (step.travelMode.equals(TravelMode.TRANSIT)) {
                if (!step.transitDetails.line.vehicle.type.equals(VehicleType.SUBWAY)) {
                    sb.append("C остановки ");
                    sb.append(step.transitDetails.departureStop.name);
                    sb.append(" до остановки ");
                    sb.append(step.transitDetails.arrivalStop.name);
                    if (step.transitDetails.line.vehicle.type.equals(VehicleType.BUS)) {
                        sb.append(" на автобусе №");
                    } else if (step.transitDetails.line.vehicle.type.equals(VehicleType.TRAM)) {
                        sb.append(" на трамвае №");
                    } else if (step.transitDetails.line.vehicle.type.equals(VehicleType.TROLLEYBUS)) {
                        sb.append(" на троллейбусе №");
                    } else {
                        sb.append(" на маршруте №");
                    }
                    sb.append(step.transitDetails.line.shortName);
                } else if (step.transitDetails.line.vehicle.type.equals(VehicleType.SUBWAY)) {
                    sb.append("Cо станции ");
                    sb.append(step.transitDetails.departureStop.name);
                    sb.append(" до станции ");
                    sb.append(step.transitDetails.arrivalStop.name);
                }

            } else if (step.travelMode.equals(TravelMode.DRIVING)) {
                if (step.steps != null) {
                    for (DirectionsStep innerStep : step.steps) {
                        if (innerStep.htmlInstructions != null) {
                            sb.append(innerStep.htmlInstructions);
                            sb.append(" и проезжайте ");
                            sb.append(innerStep.distance);
                            sb.append(", ");
                        }
                    }
                }
            }
            res.add(sb.toString());
        }
        return res;
    }

}
