package ru.vsu.travel_app_back.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.vsu.travel_app_back.domain.InputEntity;
import ru.vsu.travel_app_back.domain.LatLong;
import ru.vsu.travel_app_back.domain.Route;
import ru.vsu.travel_app_back.domain.news.Document;
import ru.vsu.travel_app_back.domain.sights.CityEntity;
import ru.vsu.travel_app_back.domain.sights.SightAnswerEntity;
import ru.vsu.travel_app_back.errors.TravelException;
import ru.vsu.travel_app_back.service.TravelService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Controller
@RequiredArgsConstructor
@RequestMapping("/v1/travelapp")
public class TravelController {
    private final TravelService service;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/calculate")
    public ResponseEntity<Route> calculateRoute(@RequestBody InputEntity data) throws TravelException {
        return new ResponseEntity<>(service.analyzeData(data), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/news/{page}")
    public ResponseEntity<List<Document>> getNews(@PathVariable int page) {
        return new ResponseEntity<>(service.getNews(page), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/health")
    public ResponseEntity<Map<String, String>> healthCheck() {
        Map<String, String> map = new HashMap<>();
        map.put("status", "UP");
        return new ResponseEntity<>(map, HttpStatus.OK );
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/route")
    public ResponseEntity saveRoute(@RequestBody Route route) throws Exception {
        service.saveRoute(route);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/route/{userId}")
    public ResponseEntity<List<Route>> getRoutesByUserId(@PathVariable("userId") String userId) throws ExecutionException, InterruptedException {
        return new ResponseEntity<>(service.getRoutes(userId), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/route/{userId}/{routeId}")
    public ResponseEntity deleteRoute(@PathVariable("userId") String userId,
                                      @PathVariable("routeId") String routeId) {
        service.deleteRoute(userId, routeId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/route/{userId}/{routeId}")
    public ResponseEntity<Route> getRoute(@PathVariable("userId") String userId,
                                          @PathVariable("routeId") String routeId) throws ExecutionException, InterruptedException {
        return new ResponseEntity<>(service.getRouteById(userId, routeId), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/city")
    public ResponseEntity<LatLong> getCityCoordinates(@RequestBody CityEntity city) {
        return new ResponseEntity<>(service.getCityCoordinatesByName(city), HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/objects")
    public ResponseEntity<List<SightAnswerEntity>> getCityObjects(@RequestParam("lat") Double lat,
                                                                  @RequestParam("lng") Double lng) {
        return new ResponseEntity<>(service.getCityObjects(new LatLong(lat, lng)), HttpStatus.OK);
    }
}

