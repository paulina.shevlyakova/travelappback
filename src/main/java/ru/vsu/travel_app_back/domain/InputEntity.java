package ru.vsu.travel_app_back.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputEntity  implements Serializable {
    /**
     * кол-во дней в путешествии
     */
    private int days;

    /**
     * стартовая точка каждого дня
     */
    private LatLong home;

    /**
     * массив координат для посещения
     */
    private List<LatLong> coordinates;
}
