package ru.vsu.travel_app_back.domain.distance_matrix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatrixElement {
    private Value distance;
    private Value duration;
    private String status;
}
