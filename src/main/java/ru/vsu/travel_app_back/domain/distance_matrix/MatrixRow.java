package ru.vsu.travel_app_back.domain.distance_matrix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatrixRow {
    private List<MatrixElement> elements;
}
