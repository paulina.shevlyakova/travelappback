package ru.vsu.travel_app_back.domain.distance_matrix;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistanceMatrixEntity {

    private String[] origin_addresses;
    private String[] destination_addresses;
    private List<MatrixRow> rows;
    private String status;
}
