package ru.vsu.travel_app_back.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import ru.vsu.travel_app_back.errors.TravelException;
import ru.vsu.travel_app_back.service.utils.Utils;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LatLong implements Serializable {
    private Double lat;
    private Double lng;
    private int number;

    public LatLong(LatLong another) {
        this.lat = another.getLat();
        this.lng = another.getLng();
    }

    public LatLong(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", lat.toString(), lng.toString());
    }

    public static Double distance(LatLong first, LatLong second) throws TravelException {
        Utils.checkGeoPoint(first);
        Utils.checkGeoPoint(second);

        double R = 6371;
        double lat = Utils.degreesToRadians(second.getLat() - first.getLat());
        double lon = Utils.degreesToRadians(second.getLng() - first.getLng());

        double a = Math.sin(lat / 2) * Math.sin(lat / 2) +
                Math.cos(Utils.degreesToRadians(first.getLat())) *
                        Math.cos(Utils.degreesToRadians(second.getLat())) *
                        Math.sin(lon / 2) * Math.sin(lon / 2);
        return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * R;
    }


}
