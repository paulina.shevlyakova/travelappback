package ru.vsu.travel_app_back.domain.sights;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeatureEntity {
    private String type;
    private String id;
    private SightGeometry geometry;
    private SightProperties properties;
}
