package ru.vsu.travel_app_back.domain.sights;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityAnswerEntity {
    private double lat;
    private double lon;
}
