package ru.vsu.travel_app_back.domain.sights;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.vsu.travel_app_back.domain.LatLong;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SightAnswerEntity {
    private String name;
    private LatLong coordinate;
    private String category;
}
