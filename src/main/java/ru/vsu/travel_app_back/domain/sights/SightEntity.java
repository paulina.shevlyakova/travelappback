package ru.vsu.travel_app_back.domain.sights;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SightEntity {
    private String type;
    private List<FeatureEntity> features;
}
