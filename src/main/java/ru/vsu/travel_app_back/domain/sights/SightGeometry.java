package ru.vsu.travel_app_back.domain.sights;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SightGeometry {
    private List<Double> coordinates;
}
