package ru.vsu.travel_app_back.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.vsu.travel_app_back.service.clustering.Cluster;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Route {
    private String userId;
    private LatLong home;
    private String routeId;
    private String name;
    private List<Cluster> clusters;
}
