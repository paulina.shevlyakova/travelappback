package ru.vsu.travel_app_back.domain.news;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Document {
    private String snippet;
    private String web_url;
    private String lead_paragraph;
    private String image_url;
    private List<Multimedia> multimedia;
}
