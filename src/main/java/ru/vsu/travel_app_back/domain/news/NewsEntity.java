package ru.vsu.travel_app_back.domain.news;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewsEntity {
    private String status;
    private String copyright;
    private Response response;

}
