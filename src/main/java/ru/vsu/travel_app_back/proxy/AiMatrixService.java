package ru.vsu.travel_app_back.proxy;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vsu.travel_app_back.domain.distance_matrix.DistanceMatrixEntity;
import ru.vsu.travel_app_back.errors.FeignPropagateBadRequestsConfiguration;

@Component
@FeignClient(name = "distanceService",
        url = "${ai.api.url}",
        configuration = FeignPropagateBadRequestsConfiguration.class
)
public interface AiMatrixService {
    @GetMapping(path = "/distancematrix/json")
    DistanceMatrixEntity calculateMatrix(@RequestParam("origins") String origins,
                                         @RequestParam("destinations") String destinations,
                                         @RequestParam("key") String key);
}
