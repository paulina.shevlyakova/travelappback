package ru.vsu.travel_app_back.proxy;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vsu.travel_app_back.domain.news.NewsEntity;
import ru.vsu.travel_app_back.errors.FeignPropagateBadRequestsConfiguration;

@Component
@FeignClient(name = "newsService",
        url = "${news.api.url}",
        configuration = FeignPropagateBadRequestsConfiguration.class
)
public interface NewsService {
    @GetMapping(path = "/articlesearch.json")
    NewsEntity getArticles(@RequestParam("q") String query,
                           @RequestParam("api-key") String apiKey,
                           @RequestParam("page") int page);
}
