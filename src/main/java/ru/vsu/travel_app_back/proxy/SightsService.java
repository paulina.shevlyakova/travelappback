package ru.vsu.travel_app_back.proxy;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vsu.travel_app_back.domain.sights.CityAnswerEntity;
import ru.vsu.travel_app_back.domain.sights.SightEntity;
import ru.vsu.travel_app_back.errors.FeignPropagateBadRequestsConfiguration;

@Component
@FeignClient(name = "sightsService",
        url = "${sights.api.url}",
        configuration = FeignPropagateBadRequestsConfiguration.class
)
public interface SightsService {
    @GetMapping(path = "/places/geoname")
    CityAnswerEntity getCityCoordinates(@RequestParam("name") String name,
                                        @RequestParam("apikey") String apiKey);

    @GetMapping(path = "/places/radius")
    SightEntity getCityObjects(@RequestParam("radius") String radius,
                                     @RequestParam("lon") String lon,
                                     @RequestParam("lat") String lat,
                                     @RequestParam("kinds") String kinds,
                                     @RequestParam("apikey") String apiKey);
}
